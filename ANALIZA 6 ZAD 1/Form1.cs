﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ANALIZA_6_ZAD_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out left))
                MessageBox.Show("Invalid input!", "Error");
            else
            {
                ResultLBL.Text = String.Format(Math.Sqrt(left) % 1 == 0 ? "{0:0}" : "{0:0.00000}", Math.Sqrt(left));
                label2.Text = "Sqrt(";
                label1.Text = ")";
            }
        }
        double left=0, right=0;

        private void BttnMinus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out left) || !double.TryParse(textBox2.Text, out right))
                MessageBox.Show("Invalid input!", "Error");
            else
            {
                ResultLBL.Text = (left - right).ToString();
                label1.Text = "-";
            }
        }

        private void BttnTimes_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out left) || !double.TryParse(textBox2.Text, out right))
                MessageBox.Show("Invalid input!", "Error");
            else
            {
                ResultLBL.Text = (left * right).ToString();
                label1.Text = "*";
            }
        }

        private void BttnDevide_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out left) || !double.TryParse(textBox2.Text, out right))
                MessageBox.Show("Invalid input!", "Error");
            else
            {
                ResultLBL.Text = (left / right).ToString();
                label1.Text = "/";
            }
        }

        private void BttnSin_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out left))
                MessageBox.Show("Invalid input!", "Error");
            else
            {
                ResultLBL.Text = String.Format(Math.Sin(left) % 1 == 0 ? "{0:0}" : "{0:0.00000}", Math.Sin(left));
                label2.Text="Sin(";
                label1.Text = ")";
            }
        }

        private void BttnCos_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out left))
                MessageBox.Show("Invalid input!", "Error");
            else
            {
                ResultLBL.Text = String.Format(Math.Cos(left) % 1 == 0 ? "{0:0}" : "{0:0.00000}", Math.Cos(left));
                label2.Text = "Cos(";
                label1.Text = ")";
            }
        }

        private void BttnPow_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out left) || !double.TryParse(textBox2.Text, out right))
                MessageBox.Show("Invalid input!", "Error");
            else
            {
                ResultLBL.Text = String.Format(Math.Pow(left,right) % 1 == 0 ? "{0:0}" : "{0:0.00000}", Math.Pow(left,right));
                label1.Text = "^";
            }
        }

        private void BttnLog_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out left))
                MessageBox.Show("Invalid input!", "Error");
            else
            {
                ResultLBL.Text = String.Format(Math.Log10(left) % 1 == 0 ? "{0:0}" : "{0:0.00000}", Math.Log10(left));
                label2.Text = "Log(";
                label1.Text = ")";
            }
        }

        private void BttnPlus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out left) || !double.TryParse(textBox2.Text, out right))
                MessageBox.Show("Invalid input!", "Error");
            else
            {
                ResultLBL.Text = (left + right).ToString();
                label1.Text = "+";
            }
        }
    }
}
