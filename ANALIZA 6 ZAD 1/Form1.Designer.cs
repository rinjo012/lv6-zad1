﻿namespace ANALIZA_6_ZAD_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BttnPlus = new System.Windows.Forms.Button();
            this.BttnMinus = new System.Windows.Forms.Button();
            this.BttnSin = new System.Windows.Forms.Button();
            this.BttnCos = new System.Windows.Forms.Button();
            this.BttnDevide = new System.Windows.Forms.Button();
            this.BttnTimes = new System.Windows.Forms.Button();
            this.BttnLog = new System.Windows.Forms.Button();
            this.BttnPow = new System.Windows.Forms.Button();
            this.BttnSqrt = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ResultLBL = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BttnPlus
            // 
            this.BttnPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BttnPlus.Location = new System.Drawing.Point(12, 246);
            this.BttnPlus.Name = "BttnPlus";
            this.BttnPlus.Size = new System.Drawing.Size(100, 51);
            this.BttnPlus.TabIndex = 0;
            this.BttnPlus.Text = "+";
            this.BttnPlus.UseVisualStyleBackColor = true;
            this.BttnPlus.Click += new System.EventHandler(this.BttnPlus_Click);
            // 
            // BttnMinus
            // 
            this.BttnMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BttnMinus.Location = new System.Drawing.Point(156, 246);
            this.BttnMinus.Name = "BttnMinus";
            this.BttnMinus.Size = new System.Drawing.Size(100, 51);
            this.BttnMinus.TabIndex = 1;
            this.BttnMinus.Text = "-";
            this.BttnMinus.UseVisualStyleBackColor = true;
            this.BttnMinus.Click += new System.EventHandler(this.BttnMinus_Click);
            // 
            // BttnSin
            // 
            this.BttnSin.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BttnSin.Location = new System.Drawing.Point(316, 246);
            this.BttnSin.Name = "BttnSin";
            this.BttnSin.Size = new System.Drawing.Size(100, 51);
            this.BttnSin.TabIndex = 2;
            this.BttnSin.Text = "sin";
            this.BttnSin.UseVisualStyleBackColor = true;
            this.BttnSin.Click += new System.EventHandler(this.BttnSin_Click);
            // 
            // BttnCos
            // 
            this.BttnCos.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BttnCos.Location = new System.Drawing.Point(316, 330);
            this.BttnCos.Name = "BttnCos";
            this.BttnCos.Size = new System.Drawing.Size(100, 51);
            this.BttnCos.TabIndex = 5;
            this.BttnCos.Text = "cos";
            this.BttnCos.UseVisualStyleBackColor = true;
            this.BttnCos.Click += new System.EventHandler(this.BttnCos_Click);
            // 
            // BttnDevide
            // 
            this.BttnDevide.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BttnDevide.Location = new System.Drawing.Point(156, 330);
            this.BttnDevide.Name = "BttnDevide";
            this.BttnDevide.Size = new System.Drawing.Size(100, 51);
            this.BttnDevide.TabIndex = 4;
            this.BttnDevide.Text = "/";
            this.BttnDevide.UseVisualStyleBackColor = true;
            this.BttnDevide.Click += new System.EventHandler(this.BttnDevide_Click);
            // 
            // BttnTimes
            // 
            this.BttnTimes.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BttnTimes.Location = new System.Drawing.Point(12, 330);
            this.BttnTimes.Name = "BttnTimes";
            this.BttnTimes.Size = new System.Drawing.Size(100, 51);
            this.BttnTimes.TabIndex = 3;
            this.BttnTimes.Text = "*";
            this.BttnTimes.UseVisualStyleBackColor = true;
            this.BttnTimes.Click += new System.EventHandler(this.BttnTimes_Click);
            // 
            // BttnLog
            // 
            this.BttnLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BttnLog.Location = new System.Drawing.Point(316, 420);
            this.BttnLog.Name = "BttnLog";
            this.BttnLog.Size = new System.Drawing.Size(100, 51);
            this.BttnLog.TabIndex = 8;
            this.BttnLog.Text = "log";
            this.BttnLog.UseVisualStyleBackColor = true;
            this.BttnLog.Click += new System.EventHandler(this.BttnLog_Click);
            // 
            // BttnPow
            // 
            this.BttnPow.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BttnPow.Location = new System.Drawing.Point(156, 420);
            this.BttnPow.Name = "BttnPow";
            this.BttnPow.Size = new System.Drawing.Size(100, 51);
            this.BttnPow.TabIndex = 7;
            this.BttnPow.Text = "x^y";
            this.BttnPow.UseVisualStyleBackColor = true;
            this.BttnPow.Click += new System.EventHandler(this.BttnPow_Click);
            // 
            // BttnSqrt
            // 
            this.BttnSqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BttnSqrt.Location = new System.Drawing.Point(12, 420);
            this.BttnSqrt.Name = "BttnSqrt";
            this.BttnSqrt.Size = new System.Drawing.Size(100, 51);
            this.BttnSqrt.TabIndex = 6;
            this.BttnSqrt.Text = "sqrt";
            this.BttnSqrt.UseVisualStyleBackColor = true;
            this.BttnSqrt.Click += new System.EventHandler(this.button9_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(111, 45);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 38);
            this.textBox1.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(217, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 32);
            this.label1.TabIndex = 11;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(257, 45);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 38);
            this.textBox2.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 32);
            this.label2.TabIndex = 13;
            // 
            // ResultLBL
            // 
            this.ResultLBL.AutoSize = true;
            this.ResultLBL.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResultLBL.Location = new System.Drawing.Point(251, 140);
            this.ResultLBL.Name = "ResultLBL";
            this.ResultLBL.Size = new System.Drawing.Size(126, 32);
            this.ResultLBL.TabIndex = 14;
            this.ResultLBL.Text = "RESULT";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 521);
            this.Controls.Add(this.ResultLBL);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.BttnLog);
            this.Controls.Add(this.BttnPow);
            this.Controls.Add(this.BttnSqrt);
            this.Controls.Add(this.BttnCos);
            this.Controls.Add(this.BttnDevide);
            this.Controls.Add(this.BttnTimes);
            this.Controls.Add(this.BttnSin);
            this.Controls.Add(this.BttnMinus);
            this.Controls.Add(this.BttnPlus);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BttnPlus;
        private System.Windows.Forms.Button BttnMinus;
        private System.Windows.Forms.Button BttnSin;
        private System.Windows.Forms.Button BttnCos;
        private System.Windows.Forms.Button BttnDevide;
        private System.Windows.Forms.Button BttnTimes;
        private System.Windows.Forms.Button BttnLog;
        private System.Windows.Forms.Button BttnPow;
        private System.Windows.Forms.Button BttnSqrt;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label ResultLBL;
    }
}

